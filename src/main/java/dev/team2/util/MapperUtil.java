package dev.team2.util;

import java.lang.reflect.Field;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.UUID;

import dev.team2.models.*;

public class MapperUtil {
	/**
     * What it does:
     * sets parameters for a preparedStatement
     *
     * What it returns:
     * int for success
     *
     * @param ps: prepared statement to insert into
     * @param fields: fields to be inserted
     * @return int
     */
    public static int setParameters (PreparedStatement ps, Object ...fields) throws SQLException {
        if (ps == null || fields == null) {
            return 0;
        }

        int i = 1;
        for (Object value : fields) {
            if (value instanceof Boolean) {
                ps.setBoolean(i++, (Boolean) value);
            } else

            if (value instanceof Byte) {
                ps.setByte(i++, (Byte) value);
            } else

            if (value instanceof Short) {
                ps.setShort(i++, (Short) value);
            } else

            if (value instanceof Integer) {
                ps.setInt(i++, (int) value);
            } else

            if (value instanceof Long) {
                ps.setLong(i++, (Long) value);
            } else

            if (value instanceof Float) {
                ps.setFloat(i++, (Float) value);
            } else

            if (value instanceof Double) {
                ps.setDouble(i++, (Double) value);
            } else

            if (value instanceof String) {
                ps.setString(i++, (String) value);

            } else {
                return 0;
            }
        }

        return i;
    }
    
    /**
     * What it does:
     * Gets corresponding class from table
     *
     * What it returns:
     * Class of table
     *
     * @param tableName: table to select from
     * @return Class
     */
    public static Class getClass (String tableName){
    	if (tableName == null) {
    		return null;
    	}
    	
    	if (tableName.equals("email_table")) {
    		return Email.class;
    	}
    	
    	if (tableName.equals("identity_table")) {
    		return Identity.class;
    	}
    	
    	if (tableName.equals("password_table")) {
    		return Password.class;
    	}
    	
    	if (tableName.equals("admin_table")) {
    		return Admin.class;
    	}
    	return null;
    }
    
    /**
     * What it does:
     * gets table name from corresponding class
     *
     * What it returns:
     * String of name of table
     *
     * @param currentClass: class of 
     * @return string
     */
    public static String getTable (Class currentClass){
    	if (currentClass == null) {
    		return null;
    	}
    	
    	if (currentClass.equals(Email.class)) {
    		return "email_table";
    	}
    	
    	if (currentClass.equals(Identity.class)) {
    		return "identity_table";
    	}
    	
    	if (currentClass.equals(Password.class)) {
    		return "password_table";
    	}
    	
    	if (currentClass.equals(Admin.class)) {
    		return "admin_table";
    	}
    	return null;
    }
    /*attempt at method with dynamic return type
    public static <T> T getParameters (ResultSet rs, Field fields, String column) throws SQLException {
        if (rs == null || fields == null) {
            return null;
        }
            if (fields.getType() == boolean.class) {
                rs.getBoolean(column);
            } else

            if (fields.getType() == Byte.class) {
                rs.getByte(column);
            } else

            if (fields.getType() == Short.class) {
            	rs.getShort(column);
            } else

            if (fields.getType() == Integer.class) {
            	rs.getInt(column);
            } else

            if (fields.getType() == Long.class) {
            	rs.getLong(column);
            } else

            if (fields.getType() == Float.class) {
            	rs.getFloat(column);
            } else

            if (fields.getType() == Double.class) {
            	rs.getDouble(column);
            } else

            if (fields.getType() == String.class) {
            	rs.getString(column);
            } else {
                return null;
            }
       
        return null;
    }
    */
}
