package dev.team2;

import dev.team2.util.ConnectionUtil;
import dev.team2.util.MapperUtil;

import java.util.*;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class TapewORM implements Mapper {
    private Connection connection;

    public Connection getConnection() {
		return connection;
	}

	public void setConnection(Connection connection) {
		this.connection = connection;
	}

	public TapewORM() {
        super();
    }

    public TapewORM(Connection connection){
        this.connection = connection;
    }
    
    /**
     * What it does:
     * INSERT INTO table VALUES (values...)
     *
     * What it returns:
     * boolean for success
     *
     * @param table: table to insert into
     * @param values: values to insert into table
     * @return boolean
     */
    public boolean add(String table, Object ...values) {
    	if (table == null || values == null) {
    		return false;
    	}
    	//
    	//StringBuilder sql = new StringBuilder("insert into " + table + " values (default, " );
    	StringBuilder sql = new StringBuilder("insert into " + table + " values (" );
    	
    	for (int i = 0; i < values.length; i++) {
    		sql.append("?, ");
    	}
    	
    	sql.setLength(sql.length()-2);
    	sql.append(")");
    	
        try (Connection connection = ConnectionUtil.getConnection();
            PreparedStatement ps = connection.prepareStatement(sql.toString());
        ) {
            MapperUtil.setParameters(ps, values);
            ps.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        	return false;
        }
    	
    	return true;
    }
    
    /**
     * What it does:
     * INSERT INTO table VALUES(current.get(...))
     *
     * What it returns:
     * boolean for success
     *
     * @param current: object that will be inserted that matches the table
     * @return boolean
     */
    public boolean add(Object current) {
    	if (current == null) {
    		return false;
    	}
    	String table = MapperUtil.getTable(current.getClass());
    	//StringBuilder sql = new StringBuilder("insert into " + table + " values (default, " );
    	StringBuilder sql = new StringBuilder("insert into " + table + " values (" );
    	
    	
    	Field[] fields = current.getClass().getDeclaredFields();
    	List<Object> values = new ArrayList<Object>();
    	for (int i = 0; i < fields.length; i++) {
    		sql.append("?, ");
    	}
    	
    	sql.setLength(sql.length()-2);
    	sql.append(")");
        try (Connection connection = ConnectionUtil.getConnection();
            PreparedStatement ps = connection.prepareStatement(sql.toString());
        	
        ) {
        	for (int i = 0; i < fields.length; i++) {
        		fields[i].setAccessible(true);
        		values.add(fields[i].get(current));
        	}
        	MapperUtil.setParameters(ps, values.toArray());
            ps.executeUpdate();
        } catch (SQLException | IllegalArgumentException | IllegalAccessException throwables) {
            throwables.printStackTrace();
        	return false;
        }
    	
    	return true;
    }

    /**
     * What it does:
     * INSERT INTO table VALUES (default, values...)
     *
     * What it returns:
     * boolean for success
     *
     * @param table: table to insert into
     * @param values: values to insert into table
     * @return boolean
     */
    public boolean addDefault(String table, Object ...values) {
    	if (table == null || values == null) {
    		return false;
    	}
    	//
    	//StringBuilder sql = new StringBuilder("insert into " + table + " values (default, " );
    	StringBuilder sql = new StringBuilder("insert into " + table + " values (default, " );
    	
    	for (int i = 0; i < values.length; i++) {
    		sql.append("?, ");
    	}
    	
    	sql.setLength(sql.length()-2);
    	sql.append(")");
    	
        try (Connection connection = ConnectionUtil.getConnection();
            PreparedStatement ps = connection.prepareStatement(sql.toString());
        ) {
            MapperUtil.setParameters(ps, values);
            ps.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        	return false;
        }
    	
    	return true;
    }
    
    /**
     * What it does:
     * SELECT * FROM table WHERE key = keyValues
     *
     * What it returns:
     * List of Object of the table
     *
     * @param table: table to select from
     * @param key: key (column) to select from
     * @param keyValues: key values of cell
     * @return List<T>
     */
    public <T> List<T> get(String table, String key, Object keyValues) {
        if (table == null || key == null || keyValues == null) {
            return null;
        }

        StringBuilder sql = new StringBuilder
        		("select * from " + table + " where " + key + " = ?");

        try (Connection connection = ConnectionUtil.getConnection();
            PreparedStatement ps = connection.prepareStatement(sql.toString());
        ) {
            MapperUtil.setParameters(ps, keyValues);
            Class<T> clazz = MapperUtil.getClass(table);
            ResultSet rs = ps.executeQuery();
            List<T> replyList = new ArrayList<T>();
            Field[] fields = clazz.getDeclaredFields();
            T reply = clazz.newInstance();
            while (rs.next()) {
            	for(Field f: fields){
            		f.setAccessible(true);
                	f.set(reply, rs.getObject(f.getName()));
                	replyList.add(reply);
            	}
            }
            return replyList;

        } catch (SQLException| InstantiationException | IllegalAccessException | 
        		SecurityException | IllegalArgumentException throwables) {
        	throwables.printStackTrace();
        	return null;
        }
    }

    /**
     * What it does:
     * SELECT columns... FROM table WHERE key = keyValues
     *
     * What it returns:
     * List of Objects of the table with cell data
     *
     * @param table: table to select from
     * @param key: key (column) to select from
     * @param keyValues: key values of cell
     * @param columns: cell data to be returned
     * @return List<T>
     */
    public <T> List<T> getColumns(String table, String key, Object keyValues, String ...columns) {
        if (table == null || columns == null || key == null || keyValues == null) {
            return null;
        }

        StringBuilder sql = new StringBuilder
        		("select ");
        for (int i = 0; i < columns.length; i++) {
    		sql.append(columns[i] + ", ");
    	}
        sql.setLength(sql.length()-2);
        sql.append(" from " + table + " where " + key + " = ?");

        try (Connection connection = ConnectionUtil.getConnection();
            PreparedStatement ps = connection.prepareStatement(sql.toString());
        ) {
            MapperUtil.setParameters(ps, keyValues);
            Class<T> clazz = MapperUtil.getClass(table);
            List<T> replyList = new ArrayList<T>();
            T reply = clazz.newInstance();
            

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
            	for (int i = 0; i < columns.length; i++) {
            		Field f = clazz.getDeclaredField(columns[i]);
            		f.setAccessible(true);
                	f.set(reply, rs.getObject(columns[i]));
                	replyList.add(reply);
            	}
            }
            return replyList;

        } catch (SQLException| InstantiationException | IllegalAccessException | 
        		NoSuchFieldException | SecurityException | IllegalArgumentException throwables) {
        	throwables.printStackTrace();
        	return null;
        }
    }
    
    /**
     * What it does:
     * SELECT columns... FROM table
     *
     * What it returns:
     * List of Objects of the table with cell data
     *
     * @param table: table to select from
     * @param columns: cell data to be returned
     * @return List<T>
     */
    public <T> List<T> getNoWhere(String table, String ...columns) {
        if (table == null || columns == null) {
            return null;
        }

        StringBuilder sql = new StringBuilder
        		("select ");
        for (int i = 0; i < columns.length; i++) {
    		sql.append(columns[i] + ", ");
    	}
        sql.setLength(sql.length()-2);
        sql.append(" from " + table);

        try (Connection connection = ConnectionUtil.getConnection();
            PreparedStatement ps = connection.prepareStatement(sql.toString());
        ) {
        	Class<T> clazz = MapperUtil.getClass(table);
            ResultSet rs = ps.executeQuery();
            List<T> replyList = new ArrayList<T>();
            
            while (rs.next()) {
            	T reply = clazz.newInstance();
            	for (int i = 0; i < columns.length; i++) {
            		Field f = clazz.getDeclaredField(columns[i]);
            		f.setAccessible(true);
                	f.set(reply, rs.getObject(columns[i]));
            	}
            	
            	replyList.add(reply);
            }
            return replyList;

        } catch (SQLException| InstantiationException | IllegalAccessException | 
        		NoSuchFieldException | SecurityException | IllegalArgumentException throwables) {
        	throwables.printStackTrace();
        	return null;
        }
    }
    
    /**
     * What it does:
     * SELECT * FROM table
     *
     * What it returns:
     * All Objects in table
     *
     * @param table: table to select from
     * @return List<T>
     */
    public <T> List<T> getAll(String table) {
    	if (table == null) {
            return null;
        }

        String sql = "select * from " + table;

        try (Connection connection = ConnectionUtil.getConnection();
            PreparedStatement ps = connection.prepareStatement(sql.toString());
        ) {
        	Class<T> clazz = MapperUtil.getClass(table);
            ResultSet rs = ps.executeQuery();
            List<T> replyList = new ArrayList<T>();
            Field[] fields = clazz.getDeclaredFields();
            
            while (rs.next()) {
            	T reply = clazz.newInstance();
            	for(Field f: fields){
            		f.setAccessible(true);
                	f.set(reply, rs.getObject(f.getName()));
            	}
            	
            	replyList.add(reply);
            }
            return replyList;

        } catch (SQLException| InstantiationException | IllegalAccessException | 
        		SecurityException | IllegalArgumentException throwables) {
        	throwables.printStackTrace();
        	return null;
        }
    }
    
    /**
     * What it does:
     * UPDATE table SET column = change WHERE key = keyValues
     *
     * What it returns:
     * boolean for success
     *
     * @param table: table to update
     * @param column: column to update
     * @param change: desired data to input
     * @param key: key (column) to select from
     * @param keyValues: key values of cell
     * @return boolean
     */
    public <T> boolean update(String table, String column, Object change ,String key, Object keyValues) {
    	if (column == null || table == null || key == null ||change == null || keyValues == null) {
    		return false;
    	}
    	StringBuilder sql = new StringBuilder
    			("update " + table + " set " + column + " = '" + change + "' where " + key + " = ?");
    	
        try (Connection connection = ConnectionUtil.getConnection();
            PreparedStatement ps = connection.prepareStatement(sql.toString());
        ) {
            MapperUtil.setParameters(ps, keyValues);
            ps.executeUpdate();
            
        } catch (SQLException throwables) {
        	throwables.printStackTrace();
        	return false;
        }
    	
    	return true;
    }
    
    /**
     * What it does:
     * UPDATE table SET column = change WHERE key = keyValues
     *
     * What it returns:
     * boolean for success
     *
     * @param current: Object to update based on corresponding table and id
     * @return boolean
     */
    public <T> boolean update(Object current) {
    	if (current == null) {
    		return false;
    	}
    	String table = MapperUtil.getTable(current.getClass());
    	String column = null;
    	Object change = null;
    	StringBuilder sql = new StringBuilder("update " + table + " set ");
    	String key = null;
    	Object keyValues = null;
    	Field[] fields = current.getClass().getDeclaredFields();
    	try {
    	fields[0].setAccessible(true);
    	key = fields[0].getName();
    	keyValues = fields[0].get(current);
    	
    	//table + " set " + column + " = '" + change + "'"
    	for (Field f : fields) {
    		f.setAccessible(true);
    		column = f.getName();
    		change = f.get(current);
    		sql.append(column + " = '" + change + "', ");
    	}
    	sql.setLength(sql.length()-2);
    	sql.append(" where " + key + " = ?");
    	}
    	catch(IllegalAccessException | SecurityException | IllegalArgumentException throwables) {
        	throwables.printStackTrace();
    	}
    	
        try (Connection connection = ConnectionUtil.getConnection();
            PreparedStatement ps = connection.prepareStatement(sql.toString());
        ) {
            MapperUtil.setParameters(ps, keyValues);
            ps.executeUpdate();
            
        } catch (SQLException throwables) {
        	throwables.printStackTrace();
        	return false;
        }
    	
    	return true;
    }
    /**
     * What it does:
     * DELETE FROM table WHERE key = keyValues
     *
     * What it returns:
     * boolean for success
     *
     * @param current: Object to delete based on corresponding table and id
     * @return boolean
     */
    public <T> boolean delete(String table, String key, Object keyValues) {
    	if (table == null || key == null || keyValues == null) {
    		return false;
    	}
    	StringBuilder sql = new StringBuilder
    			("delete from " + table + " where " + key + " = ?");
    	
        try (Connection connection = ConnectionUtil.getConnection();
            PreparedStatement ps = connection.prepareStatement(sql.toString());
        ) {
            MapperUtil.setParameters(ps, keyValues);
            ps.executeUpdate();
            
        } catch (SQLException throwables) {
        	throwables.printStackTrace();
        	return false;
        }
    	
    	return true;
    }
    
    public <T> boolean delete(Object current) {
    	if (current == null) {
    		return false;
    	}
    	String table = MapperUtil.getTable(current.getClass());
    	Field[] fields = current.getClass().getDeclaredFields();
    	String key = null;
    	Object keyValues = null;
    	
    	try {
        	fields[0].setAccessible(true);
        	key = fields[0].getName();
        	keyValues = fields[0].get(current);
    	}
        catch(IllegalAccessException | SecurityException | IllegalArgumentException throwables) {
            throwables.printStackTrace();
        }
    	
    	StringBuilder sql = new StringBuilder
    			("delete from " + table + " where " + key + " = ?");
    	
        try (Connection connection = ConnectionUtil.getConnection();
            PreparedStatement ps = connection.prepareStatement(sql.toString());
        ) {
            MapperUtil.setParameters(ps, keyValues);
            ps.executeUpdate();
            
        } catch (SQLException throwables) {
        	throwables.printStackTrace();
        	return false;
        }
    	
    	return true;
    }
}
